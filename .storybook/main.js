const path = require('path');
module.exports = {
  stories: ['../stories/**/*.stories.js'],
  addons: [
    '@storybook/addon-knobs/register',
    '@storybook/addon-notes/register',
    '@storybook/addon-viewport/register'
  ],
  webpackFinal: async (config) => {
    config.module.rules.push(
    // SCSSを読み込ませたい
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
        include: path.resolve(__dirname, '../'),
      },
      // HTMLを読み込ませたい
      {
        test: /\.html$/,
        use: ['extract-loader', 'html-loader'],
        include: path.resolve(__dirname, '../')
      }
    );

    return config;
  }
};