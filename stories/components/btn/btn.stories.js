import README from './README.md' 
import { withKnobs, text } from '@storybook/addon-knobs'
import copyCodeBlock from '@pickra/copy-code-block'
import Btn from './btn.html'
import './btn.scss'


export default {
  parameters: {
    notes: { README }
  },
  decorators: [withKnobs],
  title: 'components.Btn',
  component: Btn
}

export const HowToUse = () => {
  const tmpText = text('Text', 'Hello World!!')
  const hrefText = text('href', 'https://hogehoge.com')
  return copyCodeBlock(Btn) + `<a href="${hrefText}" class="c-button">${tmpText}</a>`
}